import React from 'react';
import './item-list.scss';
import PropTypes from 'prop-types';

const ItemList = props => {
  const { items, children: renderLabel, onItemSelected } = props;

  const data = items.map(item => {
    const { id } = item;
    const label = renderLabel(item);
    return (
      <li
        className='list-group-item'
        key={id}
        onClick={() => onItemSelected(id)}>
        {label}
      </li>
    );
  });

  return <ul className='item-list list-group'>{data}</ul>;
};

ItemList.defaultProps = {
  onItemSelected: () => {}
}

ItemList.propTypes = {
  onItemSelected: PropTypes.func,
  items: PropTypes.arrayOf(PropTypes.object).isRequired,
  children: PropTypes.func.isRequired
}

export default ItemList;
