import React from 'react';
import { SwapiServiceConsumer } from '../swapi-service-context';

const withSwapiService = (mapMethods) => (Wrapped) => {
  return props => {
    return (
      <SwapiServiceConsumer>
        { (swapiService) => {
          const serviceProps = mapMethods(swapiService);
          return <Wrapped {...props} {...serviceProps} />;
        }}
      </SwapiServiceConsumer>
    );
  };
};

export default withSwapiService;
