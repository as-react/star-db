import React, { Component } from 'react';
import ErrorIndicator from '../error-indicator';
import Spinner from '../spinner';
import ErrorBoundary from '../error-boundary';

const withData = (View) => {
    return class extends Component {
      state = {
        items: null,
        loading: true,
        error: false
      };
  
      componentDidMount() {
        this.update();
      }

      componentDidUpdate(prevProps) {
        if (this.props.getData !== prevProps.getData) {
          this.update();
        }
      }

      update() {
        this.props.getData()
          .then(items => {
            this.setState({
              items,
              loading: false
            });
          })
          .catch(this.onError);
      }
  
      onError = error => {
        this.setState({
          error: true,
          loading: false
        });
      };
  
      render() {
        const { items, loading, error } = this.state;
  
        const errorMessage = error ? <ErrorIndicator /> : null;
        const spinner = loading ? <Spinner /> : null;
  
        const content = !(loading || error) ? (
          <View {...this.props} items={items} />
        ) : null;
  
        return (
          <ErrorBoundary>
            {errorMessage}
            {spinner}
            {content}
          </ErrorBoundary>
        );
      }
    };
  };

  export default withData;