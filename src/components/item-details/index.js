import React, { Component } from 'react';
import './item-details.scss';
import ErrorIndicator from '../error-indicator';
import Spinner from '../spinner';
import ErrorButton from '../error-button';
import ErrorBoundary from '../error-boundary';

export default class ItemDetails extends Component {

  state = {
    item: null,
    image: null,
    loading: false,
    error: false
  };

  componentDidMount() {
    this.updateItem();
  }

  componentDidUpdate(prevProps, prevState) {
    if (this.props.id !== prevProps.id || this.props.getData !== prevProps.getData || this.props.getImageUrl !== prevProps.getImageUrl) {
      this.updateItem();
    }
  }

  updateItem = () => {
    const { id, getData, getImageUrl } = this.props;
    if (!id) {
      return;
    }

    this.setState({
      loading: true
    });

    getData(id)
      .then(item => {
        this.setState({ item, loading: false, image: getImageUrl(id) });
      })
      .catch(this.onError);
  };

  onError = error => {
    this.setState({
      error: true,
      loading: false
    });
  };

  render() {
    const { item, image, loading, error } = this.state;
    const spinner = loading ? <Spinner /> : null;

    if (!item && !loading) {
      return <span>Please select an item from list to show details</span>;
    } else {
      const errorMessage = error ? <ErrorIndicator /> : null;

      const content = !(loading || error) ? (
        <div className='item-details card'>
          <img className='item-image' src={image} alt='' />
          <div className='card-body'>
            <h4>{item.name}</h4>
            <ul className='list-group list-group-flush'>
              { 
                React.Children.map(this.props.children, (child) => {
                    return React.cloneElement(child, { item });
                })
              }
            </ul>
            <ErrorButton />
          </div>
        </div>
      ) : null;

      return (
        <React.Fragment>
          <ErrorBoundary>
            {spinner}
            {errorMessage}
            {content}
          </ErrorBoundary>
        </React.Fragment>
      );
    }
  }
}

const Record = ({ item, field, label }) => {
  return (
    <li className='list-group-item'>
      <span className='term'>{label}</span>
      <span>{item[field]}</span>
    </li>
  );
};

export { Record };
