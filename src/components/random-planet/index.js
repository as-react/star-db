import React, { Component } from 'react';
import './random-planet.scss';
import Spinner from '../spinner';
import ErrorIndicator from '../error-indicator';
import { withSwapiService } from '../hoc-helpers';
import PropTypes from 'prop-types';

class RandomPlanet extends Component {
  state = {
    planet: {},
    loading: true,
    error: false
  };
  
  static defaultProps = {
    updateInterval: 10000
  };

  static propTypes = {
    updateInterval: PropTypes.number
  }

  componentDidMount() {
    const { updateInterval } = this.props;
    this.updatePlanet();
    this.interval = setInterval(this.updatePlanet, updateInterval);
  }

  componentDidUpdate(prevProps) {
    if (
      this.props.getData !== prevProps.getData ||
      this.props.getImageUrl !== prevProps.getImageUrl
    ) {
      this.updatePlanet();
    }
  }

  componentWillUnmount() {
    clearInterval(this.interval);
  }

  onError = error => {
    this.setState({
      error: true,
      loading: false
    });
  };

  updatePlanet = () => {
    const { getData, getImageUrl } = this.props;
    const id = Math.floor(Math.random() * 25) + 2;
    // const id = 120000;
    getData(id)
      .then(planet =>
        this.setState({ planet, loading: false, image: getImageUrl(id) })
      )
      .catch(this.onError);
  };

  render() {
    const { planet, loading, error, image } = this.state;

    const errorMessage = error ? <ErrorIndicator /> : null;
    const spinner = loading ? <Spinner /> : null;
    const content = !(loading || error) ? (
      <PlanetView planet={planet} image={image} />
    ) : null;

    return (
      <div className='random-planet jumbotron rounded'>
        {errorMessage}
        {spinner}
        {content}
      </div>
    );
  }
}

const PlanetView = ({ planet, image }) => {
  const { name, population, rotationPeriod, diameter } = planet;
  return (
    <React.Fragment>
      <img className='planet-image' src={image} alt='' />
      <div>
        <h4>{name}</h4>
        <ul className='list-group list-group-flush'>
          <li className='list-group-item'>
            <span className='term'>Population</span>
            <span>{population}</span>
          </li>
          <li className='list-group-item'>
            <span className='term'>Rotation Period</span>
            <span>{rotationPeriod}</span>
          </li>
          <li className='list-group-item'>
            <span className='term'>Diameter</span>
            <span>{diameter}</span>
          </li>
        </ul>
      </div>
    </React.Fragment>
  );
};

const mapMethods = swapi => {
  return {
    getData: swapi.getPlanet,
    getImageUrl: swapi.getPlanetImage
  };
};

export default withSwapiService(mapMethods)(RandomPlanet);
