import React, { Component } from 'react';
import Header from '../header';
import RandomPlanet from '../random-planet';
import './app.scss';
import ErrorBoundary from '../error-boundary';
import { SwapiServiceProvider } from '../swapi-service-context';
import SwapiService from '../../services/swapi-service';
import DummySwapiService from '../../services/dummy-swapi-service';
import {
  PeoplePage,
  PlanetsPage,
  StarshipsPage,
  LoginPage,
  SecretPage,
} from '../pages';
import { BrowserRouter as Router, Route } from 'react-router-dom';
import { StarshipDetails } from '../sw-components';

export default class App extends Component {
  state = {
    showRandomPlanetEnabled: true,
    swapi: new DummySwapiService(),
    isLoggedIn: false
  };

  onServiceChange = () => {
    this.setState(({ swapi }) => {
      const Service =
        swapi instanceof SwapiService ? DummySwapiService : SwapiService;
      return {
        swapi: new Service(),
      };
    });
  };

  onLogin = () => {
    this.setState({isLoggedIn: true});
  }

  render() {
    const { showRandomPlanetEnabled, isLoggedIn } = this.state;

    const planet = showRandomPlanetEnabled ? <RandomPlanet /> : null;

    return (
      <ErrorBoundary>
        <SwapiServiceProvider value={this.state.swapi}>
          <Router>
            <div className='app'>
              <Header onServiceChange={this.onServiceChange} />
              {planet}
              <Route path='/' exact render={() => <h2>Welcome to StarDB</h2>} />
              <Route path='/people/:id?' component={PeoplePage} />
              <Route path='/planets' component={PlanetsPage} />
              <Route path='/starships' exact component={StarshipsPage} />
              <Route
                path='/starships/:id'
                render={({ match }) => {
                  const { id } = match.params;
                  return <StarshipDetails id={id} />;
                }}
              />
              <Route
                path='/login'
                render={() => {
                  return <LoginPage isLoggedIn={isLoggedIn} onLogin={this.onLogin}/>;
                }}
              />
              <Route
                path='/secret'
                exact
                render={() => {
                  return <SecretPage isLoggedIn={isLoggedIn}/>;
                }}
              />
            </div>
          </Router>
        </SwapiServiceProvider>
      </ErrorBoundary>
    );
  }
}
