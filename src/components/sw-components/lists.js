import React from 'react';
import ItemList from '../item-list';
import {
  withData,
  withChildFunction,
  withSwapiService,
  compose
} from '../hoc-helpers';

const renderName = ({ name }) => <span>{name}</span>;
const renderModelAndName = ({ model, name }) => (
  <span>
    {name} ({model})
  </span>
);

const mapPersonMethods = swapi => {
  return { getData: swapi.getAllPeople };
};

const mapPlanetMethods = swapi => {
  return { getData: swapi.getAllPlanets };
};

const mapStarshipMethods = swapi => {
  return { getData: swapi.getAllStarships };
};

// const PersonList = withSwapiService(mapPersonMethods)(
//   withData(
//     withChildFunction(renderName)(ItemList)
//   )
// );

const PersonList = compose(
  withSwapiService(mapPersonMethods),
  withData,
  withChildFunction(renderName)
)(ItemList);

const PlanetList = compose(
  withSwapiService(mapPlanetMethods),
  withData,
  withChildFunction(renderName)
)(ItemList);

const StarshipList = compose(
  withSwapiService(mapStarshipMethods),
  withData,
  withChildFunction(renderModelAndName)
)(ItemList);

export { PersonList, PlanetList, StarshipList };
