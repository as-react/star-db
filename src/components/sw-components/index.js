import PersonDetails from './person-details';
import PlanetDetails from './planet-details';
import StarshipDetails from './starship-details';

import {
    PersonList,
    PlanetList,
    StarshipList
} from './lists';

export {
    PersonDetails,
    PlanetDetails,
    StarshipDetails,
    PersonList,
    PlanetList,
    StarshipList
}