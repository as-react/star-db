import React from 'react';
import ItemDetails, { Record } from '../item-details';
import { withSwapiService } from '../hoc-helpers';

const PlanetDetails = (props) => {
  return (
    <ItemDetails {...props}>
      <Record field='population' label='Population' />
      <Record field='diameter' label='Diameter' />
    </ItemDetails>
  );
};

const mapMethods = (swapi) => {
  return {
    getData: swapi.getPlanet,
    getImageUrl: swapi.getPlanetImage
  }
}

export default withSwapiService(mapMethods)(PlanetDetails);
