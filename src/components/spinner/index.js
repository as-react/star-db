import React from 'react'
import './spinner.scss';

const Spinner = () => {
    return (
        <div className="loadingio-spinner-double-ring-65paq9spers">
            <div className="ldio-b4jz31a9kgo">
                <div></div>
                <div></div>
                <div><div></div></div>
                <div><div></div></div>
            </div>
        </div>
    );
}

export default Spinner;